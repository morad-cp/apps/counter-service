#!flask/bin/python
from flask import Flask, request, request_started

app = Flask(__name__)
counter = 0
@app.route('/', methods=["POST", "GET"])
def index():
    global counter
    if request.method == "GET" or request.method == "POST":
        counter+=1
        return f"Hmm, Plus 1 please \n The counter is: {counter}  "
    else:
        return str(f"The counter is: {counter} ")
if __name__ == '__main__':
    app.run(debug=True,ssl_context='adhoc',port=443,host='0.0.0.0')
