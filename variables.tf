
variable "TF_VAR_region" {
  default = "eu-west-1"
}
variable "TF_VAR_cluster_name" {
  default = "cp-poc-cluster"
}
variable "TF_VAR_cluster_version" {
  default = 1.21
}
variable "TF_VAR_instance_type" {
  default = "c5.xlarge"
}
variable "TF_VAR_instance_count" {
  default = 2
}
