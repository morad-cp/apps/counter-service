# Build and deploy python application 

## build application
    - ecr login .
    - docker build.
    - push to ecr .

## deploy app to eks 
    - update config for eks cluster 
    - apply deployment and load balancer for app.
