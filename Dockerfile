FROM python:3.9.5

WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt

EXPOSE 80
CMD ["python3", "/app/counter-service.py"]
